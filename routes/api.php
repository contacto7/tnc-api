<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'articles'], function (){
    Route::get('/index','App\Http\Controllers\ArticleController@index')
        ->name('articles.list');
    Route::get('/show/{article}','App\Http\Controllers\ArticleController@show')
        ->name('articles.show');
    Route::get('/listAll','App\Http\Controllers\ArticleController@listAll')
        ->name('articles.listAll');
    Route::post('/store','App\Http\Controllers\ArticleController@store')
        ->name('articles.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\ArticleController@update')
        ->name('articles.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\ArticleController@delete')
        ->name('articles.delete');
});

Route::group(['prefix' => 'points'], function (){
    Route::get('/index','App\Http\Controllers\PointController@index')
        ->name('points.list');
    Route::get('/show/{point}','App\Http\Controllers\PointController@show')
        ->name('points.show');
    Route::get('/listAll','App\Http\Controllers\PointController@listAll')
        ->name('points.listAll');
    Route::post('/store','App\Http\Controllers\PointController@store')
        ->name('points.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\PointController@update')
        ->name('points.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\PointController@delete')
        ->name('points.delete');
});

Route::group(['prefix' => 'types'], function (){
    Route::get('/index','App\Http\Controllers\TypeController@index')
        ->name('types.list');
    Route::get('/show/{article}','App\Http\Controllers\TypeController@show')
        ->name('types.show');
    Route::get('/listAll','App\Http\Controllers\TypeController@listAll')
        ->name('types.listAll');
    Route::post('/store','App\Http\Controllers\TypeController@store')
        ->name('types.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\TypeController@update')
        ->name('types.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\TypeController@delete')
        ->name('types.delete');
});

Route::group(['prefix' => 'keywords'], function (){
    Route::get('/index','App\Http\Controllers\KeywordController@index')
        ->name('keywords.list');
    Route::get('/show/{article}','App\Http\Controllers\KeywordController@show')
        ->name('keywords.show');
    Route::get('/listAll','App\Http\Controllers\KeywordController@listAll')
        ->name('keywords.listAll');
    Route::post('/store','App\Http\Controllers\KeywordController@store')
        ->name('keywords.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\KeywordController@update')
        ->name('keywords.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\KeywordController@delete')
        ->name('keywords.delete');
});

Route::group(['prefix' => 'polygons'], function (){
    Route::get('/index','App\Http\Controllers\PolygonController@index')
        ->name('polygons.list');
    Route::get('/show/{article}','App\Http\Controllers\PolygonController@show')
        ->name('polygons.show');
    Route::get('/listAll','App\Http\Controllers\PolygonController@listAll')
        ->name('polygons.listAll');
    Route::post('/store','App\Http\Controllers\PolygonController@store')
        ->name('polygons.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\PolygonController@update')
        ->name('polygons.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\PolygonController@delete')
        ->name('polygons.delete');
});

Route::group(['prefix' => 'authors'], function (){
    Route::get('/index','App\Http\Controllers\AuthorController@index')
        ->name('authors.list');
    Route::get('/show/{article}','App\Http\Controllers\AuthorController@show')
        ->name('authors.show');
    Route::get('/listAll','App\Http\Controllers\AuthorController@listAll')
        ->name('authors.listAll');
    Route::post('/store','App\Http\Controllers\AuthorController@store')
        ->name('authors.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\AuthorController@update')
        ->name('authors.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\AuthorController@delete')
        ->name('authors.delete');
});

Route::group(['prefix' => 'rivers'], function (){
    Route::get('/index','App\Http\Controllers\RiverController@index')
        ->name('rivers.list');
    Route::get('/show/{article}','App\Http\Controllers\RiverController@show')
        ->name('rivers.show');
    Route::get('/listAll','App\Http\Controllers\RiverController@listAll')
        ->name('rivers.listAll');
    Route::post('/authors','App\Http\Controllers\RiverController@store')
        ->name('rivers.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\RiverController@update')
        ->name('rivers.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\RiverController@delete')
        ->name('rivers.delete');
});
Route::group(['prefix' => 'basins'], function (){
    Route::get('/index','App\Http\Controllers\BasinController@index')
        ->name('basins.list');
    Route::get('/show/{article}','App\Http\Controllers\BasinController@show')
        ->name('basins.show');
    Route::get('/listAll','App\Http\Controllers\BasinController@listAll')
        ->name('basins.listAll');
    Route::post('/authors','App\Http\Controllers\BasinController@store')
        ->name('basins.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\BasinController@update')
        ->name('basins.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\BasinController@delete')
        ->name('basins.delete');
});

Route::group(['prefix' => 'categories'], function (){
    Route::get('/index','App\Http\Controllers\CategoryController@index')
        ->name('categories.list');
    Route::get('/show/{article}','App\Http\Controllers\CategoryController@show')
        ->name('categories.show');
    Route::get('/listAll','App\Http\Controllers\CategoryController@listAll')
        ->name('categories.listAll');
    Route::post('/authors','App\Http\Controllers\CategoryController@store')
        ->name('categories.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\CategoryController@update')
        ->name('categories.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\CategoryController@delete')
        ->name('categories.delete');
});

Route::group(['prefix' => 'attributes'], function (){
    Route::get('/index','App\Http\Controllers\AttributeController@index')
        ->name('attributes.list');
    Route::get('/show/{article}','App\Http\Controllers\AttributeController@show')
        ->name('attributes.show');
    Route::get('/listAll','App\Http\Controllers\AttributeController@listAll')
        ->name('attributes.listAll');
    Route::post('/authors','App\Http\Controllers\AttributeController@store')
        ->name('attributes.store');
    Route::put('/update/{samplingPoint}','App\Http\Controllers\AttributeController@update')
        ->name('attributes.update');
    Route::get('/delete/{samplingPoint}','App\Http\Controllers\AttributeController@delete')
        ->name('attributes.delete');
});


Route::group(['prefix' => 'statistics'], function (){
    Route::get('/articlesPerSubject','App\Http\Controllers\StatisticsController@articlesPerSubject')
        ->name('statistics.articlesPerSubject');
    Route::get('/articlesPerBasin','App\Http\Controllers\StatisticsController@articlesPerBasin')
        ->name('statistics.articlesPerBasin');
});


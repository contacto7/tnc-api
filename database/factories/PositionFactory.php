<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Position>
 */
class PositionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $jobTitle = fake()->jobTitle();
        $description = fake()->sentence();
        return [
            'name' => $jobTitle,
            'name_en' => $jobTitle." -en",
            'description' => $description,
            'description_en' => $description." -en",
        ];
    }
}

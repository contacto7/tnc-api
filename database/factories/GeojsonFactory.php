<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Geojson>
 */
class GeojsonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $geojsons = [
            "amazonas_metrics_1" => [
                "name" => "Amazonas Metricas 1",
                "name_en" => "Amazonas Metrics 1",
                "file" => "amazonas_metricas_1987.geojson"
            ],
            "amazonas_metrics_2" => [
                "name" => "Amazonas Metricas 2",
                "name_en" => "Amazonas Metrics 2",
                "file" => "amazonas_metricas_1993.geojson"
            ],
            "amazonas_islands_1" => [
                "name" => "Amazonas Islas 1",
                "name_en" => "Amazonas Islands 1",
                "file" => "amazonas_islands_1987.geojson"
            ],
            "amazonas_islands_2" => [
                "name" => "Amazonas Islas 2",
                "name_en" => "Amazonas Islands 2",
                "file" => "amazonas_islands_1993.geojson"
            ],
            "amazonas_erosion_1" => [
                "name" => "Amazonas Erosión 1",
                "name_en" => "Amazonas Erosion 1",
                "file" => "amazonas_erosion_deposicion_1987_1993.geojson"
            ],
            "amazonas_erosion_2" => [
                "name" => "Amazonas Erosión 2",
                "name_en" => "Amazonas Erosion 2",
                "file" => "amazonas_erosion_deposicion_1993_1999.geojson"
            ],
            "maranon_metrics_1" => [
                "name" => "Maranon Metricas 1",
                "name_en" => "Amazonas Metrics 1",
                "file" => "maranon_metricas_1987.geojson"
            ],
            "maranon_metrics_2" => [
                "name" => "Maranon Metricas 2",
                "name_en" => "Amazonas Metrics 2",
                "file" => "maranon_metricas_1993.geojson"
            ],
            "maranon_islands_1" => [
                "name" => "Maranon Islas 1",
                "name_en" => "Amazonas Islands 1",
                "file" => "maranon_islands_1987.geojson"
            ],
            "maranon_islands_2" => [
                "name" => "Maranon Islas 2",
                "name_en" => "Amazonas Islands 2",
                "file" => "maranon_islands_1993.geojson"
            ],
            "maranon_erosion_1" => [
                "name" => "Maranon Erosión 1",
                "name_en" => "Amazonas Erosion 1",
                "file" => "maranon_erosion_deposicion_1987_1993.geojson"
            ],
            "maranon_erosion_2" => [
                "name" => "Maranon Erosión 2",
                "name_en" => "Amazonas Erosion 2",
                "file" => "maranon_erosion_deposicion_1993_1999.geojson"
            ],
        ];

        $geojson = fake()->randomElement($geojsons);
        $random_int = fake()->numberBetween(1,150000);
        $name = 'Geojson de '.$geojson["name"]." - ".$random_int;
        $name_en = 'Geojson of '.$geojson["name_en"]." - ".$random_int;
        $slug = Str::slug($name);
        return [
            'slug' => $slug,
            'name' => $name,
            'name_en' => $name_en,
            'file' => $geojson["file"],
        ];
    }
}

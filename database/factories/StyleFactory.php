<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Style>
 */
class StyleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $switch = fake()->boolean();
        $point_image = null;
        if($switch){
            $point_image = fake()->imageUrl(15, 15, 'cats'); // Replace 'cats' with the category you want
        }
        return [
            'point_style' => fake()->randomElement(['style1', 'style2', 'style3']),
            'point_image' => $point_image,
            'point_background_color' => fake()->hexColor,
            'point_color' => fake()->hexColor,
            'point_width' => fake()->numberBetween(10, 20),
            'point_height' => fake()->numberBetween(10, 20),
            'point_size' => fake()->numberBetween(5, 20),
            'line_color' => fake()->hexColor,
            'line_width' => fake()->numberBetween(1, 5),
        ];
    }
}

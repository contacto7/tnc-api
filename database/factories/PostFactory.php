<?php

namespace Database\Factories;

use App\Models\Revision;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $post_types = ['Blog', 'Comment'];
        $post_type = fake()->randomElement($post_types);

        switch ($post_type){
            case 'Blog':
                $name = 'Blog of '.fake()->word();
                break;
            case 'Comment':
                $name = 'Comment from '.fake()->name();
                break;
            default:
                $name = fake()->name();
        }
        return [
            'name' => $name,
            'name_en' => $name." -en",
            'post_type' => $post_type,
            'access_type' => fake()->numberBetween(1,3),
            'current_content_version' => fake()->numberBetween(1,50),
            'revision_id' => Revision::all()->random()->id,
        ];
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Polygon>
 */
class PolygonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->sentence;
        $polygons = [
            'doi.org10.10022014WR015836-1.kml',
            'doi.org10.10022014WR015836-2.kml',
        ];
        return [
            'name' => $name,
            'file' => fake()->randomElement($polygons),
        ];
    }
}

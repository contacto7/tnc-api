<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Type>
 */
class TypeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $translations = [
            'article' => 'artículo',
            'paper' => 'paper',
            'essay' => 'ensayo',
        ];
        $type = fake()->randomElement(['article', 'paper', 'essay']);
        $type_es = $translations[$type];
        return [
            'name' => $type_es,
            'name_en' => $type,
        ];
    }
}

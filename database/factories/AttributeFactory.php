<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Attribute>
 */
class AttributeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->word;
        $name_en = $name."-en";
        $category = NULL;
        if(fake()->boolean()){
            $category = Category::all()->random()->id;
        }

        return [
            'name' => $name,
            'name_en' => $name_en,
            'category_id' => Category::where('id', '<', 5)->get()->random()->id,
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Document>
 */
class DocumentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $docs = [
            'test11.png',
            'test10.png',
            'test9.png',
            'test8.png',
            'test7.png',
            'test6.png',
            'test5.png',
            'test4.png',
            'test3.png',
            'test2.png',
            'test1.png',
            'test0.png',
            'test.png',
        ];
        $translations = [
            'sensoramiento' => 'sensoring',
            'batimetria' => 'batimetry',
            'modelamiento' => 'modeling',
            'resultados' => 'results',
            'simulacion' => 'simulation',
            'sensado' => 'sense',
            'estimacion' => 'estimation',
        ];
        $names_es = array_keys($translations);
        $relatedImagesTypes = [
            'hydro',
            'biodiversity',
            'threats',
            'conservation'
        ];
        $position = fake()->numberBetween(0, 6);
        $name_es = $names_es[$position];
        $name_en = $translations[$name_es];
        return [
            'name' => $name_es,
            'name_en' => $name_en,
            'type' => fake()->randomElement($relatedImagesTypes),
            'link' => fake()->randomElement($docs),
            'order' => fake()->numberBetween(1, 5),
            'article_id' => Article::all()->random()->id,
        ];
    }
}

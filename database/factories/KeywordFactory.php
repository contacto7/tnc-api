<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Keyword>
 */
class KeywordFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $keywords = [
            'experiment',
            'hypothesis',
            'observation',
            'analysis',
            'data',
            'conclusion',
            'variables',
            'control',
            'research',
            'methodology',
            'results',
            'interpretation',
            'theory',
            'sample',
            'procedure',
            'peer-review',
            'empirical',
            'quantitative',
            'qualitative',
            'experimentation',
            'replication',
            'statistical analysis',
            'hypothesize',
            'scientific method',
        ];

        $translations = [
            'experiment' => 'experimento',
            'hypothesis' => 'hipótesis',
            'observation' => 'observación',
            'analysis' => 'análisis',
            'data' => 'datos',
            'conclusion' => 'conclusión',
            'variables' => 'variables',
            'control' => 'control',
            'research' => 'investigación',
            'methodology' => 'metodología',
            'results' => 'resultados',
            'interpretation' => 'interpretación',
            'theory' => 'teoría',
            'sample' => 'muestra',
            'procedure' => 'procedimiento',
            'peer-review' => 'revisión por pares',
            'empirical' => 'empírico',
            'quantitative' => 'cuantitativo',
            'qualitative' => 'cualitativo',
            'experimentation' => 'experimentación',
            'replication' => 'réplica',
            'statistical analysis' => 'análisis estadístico',
            'hypothesize' => 'formular una hipótesis',
            'scientific method' => 'método científico',
        ];

        $keyword = fake()->randomElement($keywords);
        $keyword_es = $translations[$keyword];
        return [
            'name' => $keyword_es,
            'name_en' => $keyword,
        ];
    }
}

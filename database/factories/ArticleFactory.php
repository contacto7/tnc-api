<?php

namespace Database\Factories;

use App\Models\Point;
use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->sentence;
        $dois = [
            '10.1126/science.1235009',
            '10.1016/j.coal.2013.12.017',
            '10.1002/esp.2113',
            '10.1029/2008WR007016',
            '10.1029/2008WR007017',
            '10.1016/j.cageo.2005.05.006',
            '10.1002/hyp.6697',
            '10.1016/j.geomorph.2012.04.011',
            '10.1016/j.advwatres.2015.04.002',
            '10.1002/jgrf.20102',
            '10.1029/2011WR011601',
            '10.2110/jsr.2011.61',
            '10.1126/science.abj4017',
            '10.3390/w13101371',
        ];
        return [
            'name' => $name,
            'name_en' => "En: ".$name,
            'doi' => fake()->randomElement($dois),
            'abstract' => fake()->paragraphs(3, true),
            'abstract_en' => fake()->paragraphs(3, true),
            'point_order' => fake()->numberBetween(1, 5),
            'first_published_at' => fake()->dateTime,
            'citations' => fake()->numberBetween(128,599),
            'type_id' => Type::all()->random()->id,
            'status' => 1,
            'contact_name' => null,
            'contact_last_name' => null,
            'contact_email' => null,
            'contact_cellphone' => null,
        ];
    }
}

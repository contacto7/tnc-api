<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Map>
 */
class MapFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->name();
        return [
            'name' => "Mapa de ".$name,
            'name_en' => "Map of ".$name." -en",
            'description' => fake()->sentence,
            'description_en' => fake()->sentence." -en",
        ];
    }
}

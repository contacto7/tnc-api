<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Point>
 */
class PointFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $latitudeRange = ['min' => -11.3485, 'max' => -5.0125];
        $longitudeRange = ['min' => -78.3281, 'max' => -73.6771];
        return [
            'latitude' => fake()->latitude($latitudeRange['min'], $latitudeRange['max']),
            'longitude' => fake()->longitude($longitudeRange['min'], $longitudeRange['max']),
            'article_id' => Article::all()->random()->id,
            'name' => fake()->sentence
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\Revision;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PostContent>
 */
class PostContentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $post_types = ['Title', 'Description', 'Image'];
        $post_type = fake()->randomElement($post_types);

        switch ($post_type){
            case 'Title':
                $name = 'Title of '.fake()->word();
                $value = fake()->title;
                $value_en = $value."-en";
                break;
            case 'Description':
                $name = 'Descrition from '.fake()->name();
                $value = fake()->sentence;
                $value_en = $value."-en";
                break;
            case 'Image':
                $name = 'Image from '.fake()->name();
                $value = fake()->imageUrl();
                $value_en = fake()->imageUrl();
                break;
            default:
                $name = fake()->name();
        }
        $slug = Str::slug($name);
        return [
            'name' => $name,
            'name_en' => $name." -en",
            'slug' => $slug,
            'type' => $post_type,
            'value' => $value,
            'value_en' => $value_en,
            'post_id' => Post::all()->random()->id,
            'revision_id' => Revision::all()->random()->id,
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Geojson;
use App\Models\Map;
use App\Models\Style;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Layer>
 */
class LayerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->word;
        $description = fake()->sentence;
        return [
            'layer_type' => fake()->randomElement(['rivers', 'custom_pin', 'places']),
            'type' => fake()->randomElement(['rios', 'cuencas', 'mediciones']),
            'description' => $description,
            'description_en' => $description. "-en",
            'name' => "Capa de ".$name,
            'name_en' => "Layer of ".$name,
            'legend_html' => null,
            'z_order' => fake()->numberBetween(1, 10),
            'display_order' => fake()->numberBetween(1, 10),
            'geojson_id' => Geojson::all()->random()->id,
            'style_id' => Style::all()->random()->id,
            'map_id' => Map::all()->random()->id,
        ];
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('styles', function (Blueprint $table) {
            $table->id();
            $table->string('point_style')->default('dot');
            $table->string('point_image')->nullable();
            $table->string('point_background_color')->default('#000');
            $table->string('point_color')->nullable();
            $table->string('point_width')->nullable();
            $table->string('point_height')->nullable();
            $table->string('point_size')->nullable();
            $table->string('line_color')->default('#000');
            $table->string('line_width')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('styles');
    }
};

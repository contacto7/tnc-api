<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('point_polygon', function (Blueprint $table) {
            $table->unsignedBigInteger('point_id');
            $table->unsignedBigInteger('polygon_id');
            $table->timestamps();

            $table->foreign('point_id')->references('id')->on('points')->onDelete('cascade');
            $table->foreign('polygon_id')->references('id')->on('polygons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('point_polygon');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('layers', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('layer_type');
            $table->text('description');
            $table->text('description_en');
            $table->string('name');
            $table->string('name_en');
            $table->text('legend_html')->nullable();
            $table->integer('z_order')->default(1);
            $table->integer('display_order')->default(1);
            $table->unsignedBigInteger('geojson_id');
            $table->unsignedBigInteger('style_id');
            $table->unsignedBigInteger('map_id');
            $table->timestamps();

            $table->foreign('geojson_id')->references('id')->on('geojsons');
            $table->foreign('style_id')->references('id')->on('styles');
            $table->foreign('map_id')->references('id')->on('maps');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('layers');
    }
};

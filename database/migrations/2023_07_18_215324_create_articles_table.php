<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('doi');
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->text('abstract')->nullable();
            $table->text('abstract_en')->nullable();
            $table->integer('point_order')->default(1);
            $table->dateTime('first_published_at')->nullable();
            $table->integer('citations')->nullable();
            $table->unsignedBigInteger('type_id')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('contact_name')->nullable();
            $table->string('contact_last_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_cellphone')->nullable();
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('types');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('articles');
    }
};

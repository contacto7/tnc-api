<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Article;
use App\Models\Attribute;
use App\Models\Author;
use App\Models\Basin;
use App\Models\Category;
use App\Models\Document;
use App\Models\Geojson;
use App\Models\Keyword;
use App\Models\Layer;
use App\Models\Map;
use App\Models\Point;
use App\Models\Polygon;
use App\Models\Position;
use App\Models\Post;
use App\Models\PostContent;
use App\Models\Revision;
use App\Models\River;
use App\Models\Role;
use App\Models\Style;
use App\Models\Type;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Role::factory()->create([
            'id' => Role::DEVELOPER,
            'name' => 'Developer',
            'description' => 'Has the developer role',
        ]);
        Role::factory()->create([
            'id' => Role::ADMIN,
            'name' => 'Admin',
            'description' => 'Has the admin role',
        ]);
        Role::factory()->create([
            'id' => Role::WORKER,
            'name' => 'Worker',
            'description' => 'Has the Worker role',
        ]);

        User::factory(10)->create();

        User::factory()->create([
            'name' => 'Heser',
            'last_name' => 'Leon',
            'role_id' => Role::DEVELOPER,
            'email' => 'heser@mail.com',
        ]);

        Position::factory(5)->create();

        $users = User::get();
        foreach ($users as $user){
            $user->positions()->sync(Position::all()->random()->id);
        }

        Revision::factory(10)->create();

        Post::factory(10)
            ->create()
            ->each(function (Post $po) {
                PostContent::factory(2)->create([
                    'post_id'=>$po->id,
                ]);
            });

        Geojson::factory(10)->create();
        Map::factory(10)->create();
        Style::factory(10)->create();
        Layer::factory(10)->create();

        /*KEYWORDS*/
        $keywords = [
            'experiment' => 'experimento',
            'hypothesis' => 'hipótesis',
            'observation' => 'observación',
            'analysis' => 'análisis',
            'data' => 'datos',
            'conclusion' => 'conclusión',
            'variables' => 'variables',
            'control' => 'control',
            'research' => 'investigación',
            'methodology' => 'metodología',
            'results' => 'resultados',
            'interpretation' => 'interpretación',
            'theory' => 'teoría',
            'sample' => 'muestra',
            'procedure' => 'procedimiento',
            'peer-review' => 'revisión por pares',
            'empirical' => 'empírico',
            'quantitative' => 'cuantitativo',
            'qualitative' => 'cualitativo',
            'experimentation' => 'experimentación',
            'replication' => 'réplica',
            'statistical analysis' => 'análisis estadístico',
            'hypothesize' => 'formular una hipótesis',
            'scientific method' => 'método científico',
        ];

        foreach ($keywords as $k_en => $keyword){
            Keyword::factory(1)->create([
                'name' => $keyword,
                'name_en' => $k_en,
            ]);
        }
        /*KEYWORDS*/


        Author::factory(10)->create();
        Type::factory(1)->create([
            'name' => "artículo",
            'name_en' => "article",
        ]);
        Type::factory(1)->create([
            'name' => "paper",
            'name_en' => "paper",
        ]);
        Type::factory(1)->create([
            'name' => "ensayo",
            'name_en' => "essay",
        ]);


        /*DOCUMENTS*/
        $articlePortraits = [
            'agu-article.png',
            'earth-surface.png',
        ];

        $categories = [
            'hydrogeomorphology' => 'hidrogeomorfología',
            'biodiversity' => 'biodiversidad',
            'threats' => 'amenazas',
            'conservation' => 'conservación',
        ];
        foreach ($categories as $c_en => $category){
            Category::factory(1)->create([
                'name' => $category,
                'name_en' => $c_en,
            ]);
        }

        // SUB CATEGORIES
        Category::factory(1)->create([
            'id'=>5,
            'parent_category_id'=>1,
            'name' => 'Geomorphology',
            'name_en' => 'Geomorphology'
        ]);
        Category::factory(1)->create([
            'id'=>6,
            'parent_category_id'=>1,
            'name' => 'Field measurements',
            'name_en' => 'Field measurements'
        ]);
        Category::factory(1)->create([
            'id'=>7,
            'parent_category_id'=>1,
            'name' => 'Modeling',
            'name_en' => 'Modeling'
        ]);
        //// SUB SUB CATEGORIES
        Category::factory(1)->create([
            'id'=>8,
            'parent_category_id'=>6,
            'name' => 'Bathymetry',
            'name_en' => 'Bathymetry'
        ]);
        Category::factory(1)->create([
            'id'=>9,
            'parent_category_id'=>6,
            'name' => 'Hydrodynamics',
            'name_en' => 'Hydrodynamics'
        ]);
        Category::factory(1)->create([
            'id'=>10,
            'parent_category_id'=>6,
            'name' => 'Sediments',
            'name_en' => 'Sediments'
        ]);

        //ATTRIBUTES
        Attribute::factory(1)->create([
            'category_id'=>5,
            'name'=>"Initial Year",
            'name_en'=>"Initial Year",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>5,
            'name'=>"Final Year",
            'name_en'=>"Final Year",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>5,
            'name'=>"Avg Year Interval",
            'name_en'=>"Avg Year Interval",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>5,
            'name'=>"Time Scale",
            'name_en'=>"Time Scale",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>5,
            'name'=>"Morphometrics",
            'name_en'=>"Morphometrics",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>8,
            'name'=>"Date",
            'name_en'=>"Date",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>8,
            'name'=>"Regime",
            'name_en'=>"Regime",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>8,
            'name'=>"Instrument",
            'name_en'=>"Instrument",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>8,
            'name'=>"Variables",
            'name_en'=>"Variables",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>9,
            'name'=>"Date",
            'name_en'=>"Date",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>9,
            'name'=>"Regime",
            'name_en'=>"Regime",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>9,
            'name'=>"Instrument",
            'name_en'=>"Instrument",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>9,
            'name'=>"Variable",
            'name_en'=>"Variable",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>10,
            'name'=>"Date",
            'name_en'=>"Date",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>10,
            'name'=>"Regime",
            'name_en'=>"Regime",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>10,
            'name'=>"Transport Type",
            'name_en'=>"Transport Type",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>10,
            'name'=>"Material",
            'name_en'=>"Material",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>7,
            'name'=>"Type",
            'name_en'=>"Type",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>7,
            'name'=>"Process",
            'name_en'=>"Process",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>2,
            'name'=>"Taxa",
            'name_en'=>"Taxa",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>2,
            'name'=>"Initial Year",
            'name_en'=>"Initial Year",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>2,
            'name'=>"Final Year",
            'name_en'=>"Final Year",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>2,
            'name'=>"Ecological Level",
            'name_en'=>"Ecological Level",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>2,
            'name'=>"Data Source",
            'name_en'=>"Data Source",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>3,
            'name'=>"Process",
            'name_en'=>"Process",
        ]);
        Attribute::factory(1)->create([
            'category_id'=>4,
            'name'=>"Process",
            'name_en'=>"Process",
        ]);


        $rivers = [
            "Amazon" => "Amazonas",
            "Napo" => "Napo",
            "Ucayali" => "Ucayali",
            "Maranon" => "Marañon",
            "Huallaga" => "Huallaga",
            "Madeira" => "Madeira",
            "Branco" => "Branco",
            "Esmeraldas" => "Esmeraldas",
            "Santiago" => "Santiago",
            "Pastaza" => "Pastaza",
            "Araguaia" => "Araguaia",
            "Madre de Dios" => "Madre de Dios",
        ];

        $basins = [
            "Ucayali" => "Ucayali",
            "Amazon" => "Amazonas",
            "Maranon" => "Marañon",
            "Huallaga" => "Huallaga",
            "Amazon floodplain" => "Llanura amazónica",
            "Napo" => "Napo",
            "Madeira" => "Madeira",
            "Branco" => "Branco",
            "Madre de Dios" => "Madre de Dios",
        ];

        foreach ($rivers as $r_en => $river){
            River::factory(1)->create([
                'name' => $river,
                'name_en' => $r_en,
            ]);
        }
        foreach ($basins as $b_en => $basin){
            Basin::factory(1)->create([
                'name' => $basin,
                'name_en' => $b_en,
            ]);
        }

        Article::factory(1)
            ->create([
                'doi'=>'10.1111/faf.12328',
                'abstract'=>'Infrastructure development and overfishing in the Amazon make it imperative to define adequate scales for the ecosystem-based management of commercial fisheries and the wetlands on which they depend. We mapped fisheries and fish ecology data from Brazil, Peru, Bolivia and Colombia to an explicit GIS framework of river basins and mainstems. Migratory species account for more than 80% of the known maximum catches of commercial fisheries across the Amazon. Of these migratory species, we nominated six long-distance migratory fish taxa as flagship species to define the two main commercial fishery regions. The migrations of at least one goliath catfish species define a large-scale longitudinal link joining the Andes, Amazon Lowlands and Amazon River estuary. Migratory Characiforms demonstrate interbasin wetland connectivity between nutrient-rich and nutrient-poor rivers over at least 2 million km2, or about one-third of the Amazon Basin. We show that flooded forest area is the most important wetland variable explaining regional variations in migratory characiforme biomass as indicated by maximum annual fishery catches. The sustainable management of Amazon fisheries will require transnational cooperation and a paradigm shift from local community management alone to a more integrated approach that considers both rural and urban consumers and challenges, and the realistic life histories of migratory species.',
            ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -4.989055,
                    "longitude" => -75.505453,
                ]);
                $article->basins()->attach([2]);
                $article->rivers()->attach([3,4]);
                $article->categories()->attach([4]);
                $article->attributes()->attach(25, ["value"=>"No","value_en"=>"No"]);
                $article->attributes()->attach(26, ["value"=>"Yes","value_en"=>"Yes"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });

        Article::factory(1)
            ->create([
                'doi'=>'10.3389/fenvs.2023.1082619',
                'abstract'=>'The Peruvian Amazon is known for harboring the greatest biodiversity on the planet, with a world record for biodiversity per unit area. Previous studies suggested that the high ecological value depends on correlations between ecosystem functionality and seasonal inundation control vegetation patches. However, the knowledge on how river morphodynamics and its complex erosion-depositional processes influence the aquatic mosaic and fishing activity in the region is still incipient. This study examines the hydrogeomorphology of the Peruvian tropical wetland of Pacaya Samiria, located in Western Amazonia, and its role in the distribution of aquatic habitats. By using remote sensing techniques, the hydrogeomorphological connectivity that bounds the Pacaya Samiria National Reserve is characterized by ancient to modern river processes. Additionally, river signatures developed by the Ucayali, Marañon, Huallaga, Pacaya, and Samiria Rivers overlap with fish extraction and dominant vegetation to describe how geomorphology is associated with the spatial distribution of fishing zones. Results indicated that paleochannels regulate wetland drainage within the Ucamara Depression, supporting stational water stagnation, vegetation cover, and formation of carbon rich detritus, relevant aspects to understand fish traits. Moreover, the Ucayali River dominates river dynamics in the Pacaya Samiria wetland, thus playing a pivotal role in shaping the complexity of streams and lakes. Furthermore, underfit-scavenger meandering rivers are observed in areas where paleochannels from large rivers are found. A geomorphological characterization of drainage patterns in freshwater environments, such as Amazonian wetlands, is crucial to develop sound management strategies. This methodological approach is expected to support decision-making in conservation actions in Amazonian environments based on understanding wetland connectivity and hydrogeomorphological behavior and their influence on commercial fisheries.',
                 ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -5.310231,
                    "longitude" => -74.872306,
                ]);
                $article->basins()->attach([2]);
                $article->rivers()->attach([3,4]);
                $article->categories()->attach([1,2]);
                $article->attributes()->attach(1, ["value"=>1985,"value_en"=>1985]);
                $article->attributes()->attach(2, ["value"=>2021,"value_en"=>2021]);
                $article->attributes()->attach(3, ["value"=>5,"value_en"=>5]);
                $article->attributes()->attach(4, ["value"=>"Engineering,Geological","value_en"=>"Engineering,Geological"]);

                $article->attributes()->attach(20, ["value"=>"FISH","value_en"=>"FISH"]);
                $article->attributes()->attach(21, ["value"=>"2015","value_en"=>"2015"]);
                $article->attributes()->attach(22, ["value"=>"2017","value_en"=>"2017"]);
                $article->attributes()->attach(23, ["value"=>"Species, Population","value_en"=>"Species, Population"]);
                $article->attributes()->attach(24, ["value"=>"Database","value_en"=>"Database"]);
                $article->attributes()->attach(25, ["value"=>"No","value_en"=>"No"]);
                $article->attributes()->attach(26, ["value"=>"No","value_en"=>"No"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });

        Article::factory(1)
            ->create([
                'doi'=>'10.1635/0097-3157(2008)157[181:ANSOGS]2.0.CO;2',
                'abstract'=>'We describe a new species of driftwood catfish, Gelanoglanis travieso, (Siluriformes: Auchenipteridae) from the Marañon River, a whitewater tributary of the Amazon River in northeastern Perú. It shares with the two described species in this genus, G. stroudi, from left bank whitewater tributaries of the Orinoco River in Colombia and Venezuela, and G. nanonocticolus from blackwater tributaries of the upper Orinoco and Negro Rivers in Amazonas, Venezuela and northern Brazil, the following synapomorphies: reduced size, compressed body, conical snout, a single pair of mental barbels, premaxillae widely separated at rostral border of upper jaw, premaxillary and dentary tooth patches narrow, posterior naris long and narrow and positioned immediately anterior to orbit, and small eyes. Gelanoglanis travieso differs from all congeners in having second dorsal-fin lepidotrichium filamentous, simple, not a spine, and not serrate (shared with G. nanonocticolus); pectoral-fin spine stout, serrate along posterior margin (shared with G. stroudi); and a terminal mouth (vs. subterminal in G. nanonocticolus and G. stroudi)..',
                ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -5.021978,
                    "longitude" => -78.354756,
                ]);
                $article->basins()->attach([2]);
                $article->rivers()->attach([4]);
                $article->categories()->attach([2]);

                $article->attributes()->attach(20, ["value"=>"FISH","value_en"=>"FISH"]);
                $article->attributes()->attach(21, ["value"=>"1999","value_en"=>"1999"]);
                $article->attributes()->attach(23, ["value"=>"Species","value_en"=>"Species"]);
                $article->attributes()->attach(24, ["value"=>"Project based","value_en"=>"Project based"]);
                $article->attributes()->attach(25, ["value"=>"No","value_en"=>"No"]);
                $article->attributes()->attach(26, ["value"=>"No","value_en"=>"No"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });
        Article::factory(1)
            ->create([
                'doi'=>'10.1111/conl.12008',
                'abstract'=>'The hydrological connectivity of freshwater ecosystems in the Amazon basin makes them highly sensitive to a broad range of anthropogenic activities occurring in aquatic and terrestrial systems at local and distant locations. Amazon freshwater ecosystems are suffering escalating impacts caused by expansions in deforestation, pollution, construction of dams and waterways, and overharvesting of animal and plant species. The natural functions of these ecosystems are changing, and their capacity to provide historically important goods and services is declining. Existing management policies—including national water resources legislation, community-based natural resource management schemes, and the protected area network that now epitomizes the Amazon conservation paradigm—cannot adequately curb most impacts. Such management strategies are intended to conserve terrestrial ecosystems, have design and implementation deficiencies, or fail to account for the hydrologic connectivity of freshwater ecosystems. There is an urgent need to shift the Amazon conservation paradigm, broadening its current forest-centric focus to encompass the freshwater ecosystems that are vital components of the basin. This is possible by developing a river catchment-based conservation framework for the whole basin that protects both aquatic and terrestrial ecosystems..',
                ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -5.110664,
                    "longitude" => -75.586088,
                ]);
                $article->basins()->attach([2]);
                $article->rivers()->attach([1]);
                $article->categories()->attach([3,4]);
                $article->attributes()->attach(25, ["value"=>"Yes","value_en"=>"Yes"]);
                $article->attributes()->attach(26, ["value"=>"Yes","value_en"=>"Yes"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });
        Article::factory(1)
            ->create([
                'doi'=>'10.1016/j.jsames.2012.09.002',
                'abstract'=>'The erosion and transport of sediments allow us to understand many activities of significance, such as crust evolution, climate change, uplift rates, continental processes, the biogeochemical cycling of pollutants and nutrients. The Amazon basin of Peru has contrasting physiographic and climatic characteristics between the Andean piedmont and the plains and between the north and south of the basin which is why there are 8 gauging stations located along the principal rivers of the Andean piedmont (Marañón, Huallaga, Ucayali) and the plain (Marañón, Tigre, Napo, Ucayali and Amazon rivers). Since 2003, the ORE-Hybam (IRD-SENAMHI-UNALM) observatory has performed out regular measurements at strategic points of the Amazon basin to understand and model the systems, behavior and long-term dynamics. On the Andean piedmont, the suspended yields are governed by a simple model with a relationship between the river discharge and the sediment concentration. In the plain, the dilution effect of the concentrations can create hysteresis in this relationship on a monthly basis. The Amazon basin of Peru has a sediment yield of 541 *106 t year−1, 70% comes from the southern basin.',
                ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -5.033675617,
                    "longitude" => -73.83791396,
                ]);
                $article->basins()->attach([1,3,4,5,6]);
                $article->rivers()->attach([3,4,5,1,2]);
                $article->categories()->attach([1]);
                $article->attributes()->attach(1, ["value"=>"1990","value_en"=>"1990"]);
                $article->attributes()->attach(2, ["value"=>"2021","value_en"=>"2021"]);
                $article->attributes()->attach(3, ["value"=>"2","value_en"=>"2"]);
                $article->attributes()->attach(4, ["value"=>"Geological,Engineering","value_en"=>"Geological,Engineering"]);

                $article->attributes()->attach(5, ["value"=>"'Channel width, Erosion/deposition, Sinuosity'","value_en"=>"'Channel width, Erosion/deposition, Sinuosity'"]);
                $article->attributes()->attach(6, ["value"=>"02/2009","value_en"=>"02/2009"]);
                $article->attributes()->attach(7, ["value"=>"Transition LOW to HIGH, HIGH","value_en"=>"Transition LOW to HIGH, HIGH"]);
                $article->attributes()->attach(8, ["value"=>"Single beam, Multi beam","value_en"=>"Single beam, Multi beam"]);
                $article->attributes()->attach(9, ["value"=>"Dune celerity, Water depth","value_en"=>"Dune celerity, Water depth"]);
                $article->attributes()->attach(10, ["value"=>"11/2009","value_en"=>"11/2009"]);
                $article->attributes()->attach(11, ["value"=>"Transition LOW to HIGH, LOW","value_en"=>"Transition LOW to HIGH, LOW"]);
                $article->attributes()->attach(12, ["value"=>"Single beam, Multi beam","value_en"=>"Single beam, Multi beam"]);
                $article->attributes()->attach(13, ["value"=>"Water surface elevation, Flow structure","value_en"=>"Water surface elevation, Flow structure"]);
                $article->attributes()->attach(14, ["value"=>"10/2011","value_en"=>"10/2011"]);
                $article->attributes()->attach(15, ["value"=>"Transition HIGH to LOW, LOW","value_en"=>"Transition HIGH to LOW, LOW"]);
                $article->attributes()->attach(16, ["value"=>"Suspended, Bedload","value_en"=>"Suspended, Bedload"]);
                $article->attributes()->attach(17, ["value"=>"Bed, Bank","value_en"=>"Bed, Bank"]);
                $article->attributes()->attach(18, ["value"=>"Three dimensional, One dimensional","value_en"=>"Three dimensional, One dimensional"]);
                $article->attributes()->attach(19, ["value"=>"Sediment transport, HIGH","value_en"=>"Sediment transport, HIGH"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });
        Article::factory(1)
            ->create([
                'doi'=>'10.1080/02626667.2013.826359',
                'abstract'=>'The biodiversity and productivity of the Amazon floodplain depend on nutrients and organic matter transported with suspended sediments. Nevertheless, there are still fundamental unknowns about how hydrological and rainfall variability influence sediment flux in the Amazon River. To address this gap, we analyzed 3069 sediment samples collected every 10 days during 1995–2014 at five gauging stations located in the main rivers. We have two distinct fractions of suspended sediments, fine (clay and silt) and coarse (sand), which followed contrasting seasonal and long-term patterns. By taking these dynamics into account, it was estimated, for first time, in the Amazon plain, that the suspended sediment flux separately measured approximately 60% fine and 40% coarse sediment. We find that the fine suspended sediments flux is linked to rainfall and higher coarse suspended sediment flux is related with discharge. Additionally this work presents the time lag between rainfall and discharge, which is related to the upstream area of the gauging. This result is an important contribution to knowledge of biological and geomorphological issues in Amazon basin.',
                ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -3.003111697,
                    "longitude" => -78.21975159,
                ]);
                $article->basins()->attach([3]);
                $article->rivers()->attach([2,10,8,9]);
                $article->categories()->attach([1]);
                $article->attributes()->attach(1, ["value"=>"1970","value_en"=>"1970"]);
                $article->attributes()->attach(2, ["value"=>"2002","value_en"=>"2002"]);
                $article->attributes()->attach(3, ["value"=>"4","value_en"=>"4"]);
                $article->attributes()->attach(4, ["value"=>"Engineering,Geological","value_en"=>"Engineering,Geological"]);

                $article->attributes()->attach(5, ["value"=>"Migration rate, Sinuosity, Activity","value_en"=>"Migration rate, Sinuosity, Activity"]);
                $article->attributes()->attach(6, ["value"=>"11/2015","value_en"=>"11/2015"]);
                $article->attributes()->attach(7, ["value"=>"HIGH, Transition HIGH to LOW","value_en"=>"HIGH, Transition HIGH to LOW"]);
                $article->attributes()->attach(8, ["value"=>"Single beam, Multi beam","value_en"=>"Single beam, Multi beam"]);
                $article->attributes()->attach(9, ["value"=>"Bed elevation, Water depth","value_en"=>"Bed elevation, Water depth"]);
                $article->attributes()->attach(10, ["value"=>"02/2004","value_en"=>"02/2004"]);
                $article->attributes()->attach(11, ["value"=>"Transition LOW to HIGH, Transition HIGH to LOW","value_en"=>"Transition LOW to HIGH, Transition HIGH to LOW"]);
                $article->attributes()->attach(12, ["value"=>"Other, Single beam","value_en"=>"Other, Single beam"]);
                $article->attributes()->attach(13, ["value"=>"Water surface elevation, Water depth","value_en"=>"Water surface elevation, Water depth"]);
                $article->attributes()->attach(14, ["value"=>"05/2011","value_en"=>"05/2011"]);
                $article->attributes()->attach(15, ["value"=>"Transition LOW to HIGH, HIGH","value_en"=>"Transition LOW to HIGH, HIGH"]);
                $article->attributes()->attach(16, ["value"=>"Bedload, Washload","value_en"=>"Bedload, Washload"]);
                $article->attributes()->attach(17, ["value"=>"Floodplain, Bed","value_en"=>"Floodplain, Bed"]);
                $article->attributes()->attach(18, ["value"=>"Three dimensional, One dimensional","value_en"=>"Three dimensional, One dimensional"]);
                $article->attributes()->attach(19, ["value"=>"Planform morphology, HIGH","value_en"=>"Planform morphology, HIGH"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });
        Article::factory(1)
            ->create([
                'doi'=>'10.1088/2515-7620/ab9003',
                'abstract'=>'The biodiversity and productivity of the Amazon floodplain depend on nutrients and organic matter transported with suspended sediments. Nevertheless, there are still fundamental unknowns about how hydrological and rainfall variability influence sediment flux in the Amazon River. To address this gap, we analyzed 3069 sediment samples collected every 10 days during 1995–2014 at five gauging stations located in the main rivers. We have two distinct fractions of suspended sediments, fine (clay and silt) and coarse (sand), which followed contrasting seasonal and long-term patterns. By taking these dynamics into account, it was estimated, for first time, in the Amazon plain, that the suspended sediment flux separately measured approximately 60% fine and 40% coarse sediment. We find that the fine suspended sediments flux is linked to rainfall and higher coarse suspended sediment flux is related with discharge. Additionally this work presents the time lag between rainfall and discharge, which is related to the upstream area of the gauging. This result is an important contribution to knowledge of biological and geomorphological issues in Amazon basin.',
                 ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -3.705515023,
                    "longitude" => -61.45438712,
                ]);
                $article->basins()->attach([3,7,8,5]);
                $article->rivers()->attach([4,6,7,1]);
                $article->categories()->attach([1]);
                $article->attributes()->attach(1, ["value"=>"1995","value_en"=>"1995"]);
                $article->attributes()->attach(2, ["value"=>"2014","value_en"=>"2014"]);
                $article->attributes()->attach(3, ["value"=>"3","value_en"=>"3"]);
                $article->attributes()->attach(4, ["value"=>"Engineering,Geological","value_en"=>"Engineering,Geological"]);

                $article->attributes()->attach(5, ["value"=>"Angle bifurcation/confluence, Erosion/deposition, Migration rate","value_en"=>"Angle bifurcation/confluence, Erosion/deposition, Migration rate"]);
                $article->attributes()->attach(6, ["value"=>"01/2011","value_en"=>"01/2011"]);
                $article->attributes()->attach(7, ["value"=>"LOW, Transition HIGH to LOW","value_en"=>"LOW, Transition HIGH to LOW"]);
                $article->attributes()->attach(8, ["value"=>"Other, Single beam","value_en"=>"Other, Single beam"]);
                $article->attributes()->attach(9, ["value"=>"Dune wavelength, Bed elevation","value_en"=>"Dune wavelength, Bed elevation"]);
                $article->attributes()->attach(10, ["value"=>"11/2022","value_en"=>"11/2022"]);
                $article->attributes()->attach(11, ["value"=>"Transition LOW to HIGH, HIGH","value_en"=>"Transition LOW to HIGH, HIGH"]);
                $article->attributes()->attach(12, ["value"=>"Single beam, Multi beam","value_en"=>"Single beam, Multi beam"]);
                $article->attributes()->attach(13, ["value"=>"Water surface elevation, Flow structure","value_en"=>"Water surface elevation, Flow structure"]);
                $article->attributes()->attach(14, ["value"=>"11/2011","value_en"=>"11/2011"]);
                $article->attributes()->attach(15, ["value"=>"LOW, Transition HIGH to LOW","value_en"=>"LOW, Transition HIGH to LOW"]);
                $article->attributes()->attach(16, ["value"=>"Bedload, Suspended","value_en"=>"Bedload, Suspended"]);
                $article->attributes()->attach(17, ["value"=>"Bank, Bed","value_en"=>"Bank, Bed"]);
                $article->attributes()->attach(18, ["value"=>"Two dimensional, Three dimensional","value_en"=>"Two dimensional, Three dimensional"]);
                $article->attributes()->attach(19, ["value"=>"Planform morphology, HIGH","value_en"=>"Planform morphology, HIGH"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });
        Article::factory(1)
            ->create([
                'doi'=>'10.3389/frwa.2021.738527',
                'abstract'=>'The Madeira River rises in the Andes, draining the southwestern Amazon basin and contributing up to 50% of the Amazon River sediment load. The Porto Velho station monitors the Upper Madeira basin and is located just downstream of the Jirau and Santo Antonio hydropower dams. At this station, decreasing trend (p < 0.10) of the surface suspended sediment concentration (SSSC) has been documented during the sediment peak season (December to February) for the 2003–2017 period. This study aims to evaluate the role of the rainfall variability on this documented decreasing trend. For this purpose, we applied correlation and trend analysis in water discharge, SSSC and rainfall time series over the main tributaries of the Upper Madeira basin. The decline of SSSC in December is attributed to the reduction of rainfall in the Madre de Dios sub-basin from the start of the rainy season in October. However, the SSSC negative trend (p < 0.10) in January and February is associated with a shift in the magnitude of rainfall during these months in the Andean region after 2008, and the dilution associated with base flow. These results reveal that the decline of SSSC in the Madeira River should not be evaluated just on the basis of the data downstream from the dams, but also of the processes upstream in the Andean part of the basin. In a context of drastic anthropogenic climate and environmental changes, understanding the combined influence of regional hydroclimate variability and human actions on erosion and sediment transport remains a critical issue for the conservation of the Amazon-Andes system.',
                ])
            ->each(function (Article $article){
                Point::factory(1)->create([
                    'name' => $article->id,
                    "latitude" => -10.3762406,
                    "longitude" => -65.391104,
                ]);
                $article->basins()->attach([9]);
                $article->rivers()->attach([12]);
                $article->categories()->attach([1]);
                $article->attributes()->attach(1, ["value"=>"2003","value_en"=>"2003"]);
                $article->attributes()->attach(2, ["value"=>"2017","value_en"=>"2017"]);
                $article->attributes()->attach(3, ["value"=>"2","value_en"=>"2"]);
                $article->attributes()->attach(4, ["value"=>"Geological,Engineering","value_en"=>"Geological,Engineering"]);

                $article->attributes()->attach(5, ["value"=>"Sinuosity, Activity, Migration rate","value_en"=>"Sinuosity, Activity, Migration rate"]);
                $article->attributes()->attach(6, ["value"=>"11/2015","value_en"=>"11/2015"]);
                $article->attributes()->attach(7, ["value"=>"HIGH, LOW","value_en"=>"HIGH, LOW"]);
                $article->attributes()->attach(8, ["value"=>"Single beam, Multi beam","value_en"=>"Single beam, Multi beam"]);
                $article->attributes()->attach(9, ["value"=>"Water depth, Dune celerity","value_en"=>"Water depth, Dune celerity"]);
                $article->attributes()->attach(10, ["value"=>"04/2004","value_en"=>"04/2004"]);
                $article->attributes()->attach(11, ["value"=>"LOW, Transition HIGH to LOW","value_en"=>"LOW, Transition HIGH to LOW"]);
                $article->attributes()->attach(12, ["value"=>"Single beam, Multi beam","value_en"=>"Single beam, Multi beam"]);
                $article->attributes()->attach(13, ["value"=>"Flow structure, Water depth","value_en"=>"Flow structure, Water depth"]);
                $article->attributes()->attach(14, ["value"=>"08/2005","value_en"=>"08/2005"]);
                $article->attributes()->attach(15, ["value"=>"Transition LOW to HIGH, Transition HIGH to LOW","value_en"=>"Transition LOW to HIGH, Transition HIGH to LOW"]);
                $article->attributes()->attach(16, ["value"=>"Washload, Suspended","value_en"=>"Washload, Suspended"]);
                $article->attributes()->attach(17, ["value"=>"Bank, Floodplain","value_en"=>"Bank, Floodplain"]);
                $article->attributes()->attach(18, ["value"=>"Two dimensional, One dimensional","value_en"=>"Two dimensional, One dimensional"]);
                $article->attributes()->attach(19, ["value"=>"HIGH, Planform morphology","value_en"=>"HIGH, Planform morphology"]);

                $rand = random_int(1,3);$used = [];
                for ($i = 0; $i <= $rand; $i++){
                    $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                    $used[] =$keyword;
                    $article->keywords()->attach($keyword);
                }
            });

        $coordinates = [
            [-73.76551, -10.70053968],
            [-64.79281, -7.252451849],
            [-54.13345, -2.15775453],
            [-59.07566, -3.671604974],
            [-66.76315, -2.727326369],
            [-60.39882, -5.113614998],
            [-60.66417, -7.504127657],
            [-58.54451, -3.211318191],
            [-58.14510, -7.342115702],
            [-67.09852, -0.128950701],
            [-63.56770, -8.60860505],
            [-60.75842, -2.732534883],
            [-58.11286, -2.586004005],
            [-68.31367, -2.958868056],
            [-61.64401, 0.521258632]
        ];
        foreach ($coordinates as $coordinate){

            Article::factory(1)
                ->create()
                ->each(function (Article $article) use ($articlePortraits,$coordinate ){
                    Point::factory(1)->create([
                        'name' => $article->id,
                        "latitude" => $coordinate[1],
                        "longitude" => $coordinate[0],
                    ]);
                    $basin = Basin::get()->random()->id;
                    $article->basins()->attach($basin);

                    $rand = random_int(3,6);
                    $used = [];
                    for ($i = 0; $i <= $rand; $i++){
                        //KEYWORDS
                        $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                        $used[] =$keyword;
                        $article->keywords()->attach($keyword);
                    }

                    $rand = random_int(1,3);
                    $usedAuthor = [];
                    $usedRivers = [];
                    $usedCategory = [];
                    for ($i = 1; $i <= $rand; $i++){
                        //RIVERS
                        $river = River::whereNotIn('id', $usedRivers)->get()->random()->id;
                        $usedRivers[] =$river;
                        $article->rivers()->attach($river);


                        //AUTHORS
                        $author = Author::whereNotIn('id', $usedAuthor)->get()->random()->id;
                        $usedAuthor[] =$author;
                        $article->authors()->attach($author, ["position"=>$i]);


                    }
                    $usedAttribute = [];
                    for ($i = 0; $i <= 10; $i++){
                        $attribute = Attribute::whereNotIn('id', $usedAttribute)->get()->random()->id;
                        $usedAttribute[] =$attribute;

                        $attr_value = fake()->word;
                        $attr_value_en = $attr_value."-en";
                        $article->attributes()->attach($attribute, ["value"=>$attr_value,"value_en"=>$attr_value_en]);
                    }
                });
        }


        Polygon::factory(1)->create([
            'file'=>'doi.org10.10022014WR015836-1.kml',
        ]);
        Polygon::factory(1)->create([
            'file'=>'doi.org10.10022014WR015836-2.kml',
        ]);
        $points = Point::get();
        foreach ($points as $point){
            $point->polygons()->sync(Polygon::all()->random()->id);
        }

        /*
         *

        Point::factory(1)
            ->create([
                "latitude" => -4.989055,
                "longitude" => -75.505453,
            ])
            ->each(function (Point $po) {
                Article::factory(1)
                    ->create([
                        'point_id'=>$po->id,
                        'doi'=>'10.1111/faf.12328',
                        'abstract'=>'Infrastructure development and overfishing in the Amazon make it imperative to define adequate scales for the ecosystem-based management of commercial fisheries and the wetlands on which they depend. We mapped fisheries and fish ecology data from Brazil, Peru, Bolivia and Colombia to an explicit GIS framework of river basins and mainstems. Migratory species account for more than 80% of the known maximum catches of commercial fisheries across the Amazon. Of these migratory species, we nominated six long-distance migratory fish taxa as flagship species to define the two main commercial fishery regions. The migrations of at least one goliath catfish species define a large-scale longitudinal link joining the Andes, Amazon Lowlands and Amazon River estuary. Migratory Characiforms demonstrate interbasin wetland connectivity between nutrient-rich and nutrient-poor rivers over at least 2 million km2, or about one-third of the Amazon Basin. We show that flooded forest area is the most important wetland variable explaining regional variations in migratory characiforme biomass as indicated by maximum annual fishery catches. The sustainable management of Amazon fisheries will require transnational cooperation and a paradigm shift from local community management alone to a more integrated approach that considers both rural and urban consumers and challenges, and the realistic life histories of migratory species.',
                        'polygon'=>'[-9.470212440761184, -75.89250024120528, -9.782783857663583, -78.02614440852555]',
                    ])
                    ->each(function (Article $article){
                        $article->basins()->attach([2]);
                        $article->rivers()->attach([3,4]);
                        $article->categories()->attach([4]);
                        $article->attributes()->attach(25, ["value"=>"No","value_en"=>"No"]);
                        $article->attributes()->attach(26, ["value"=>"Yes","value_en"=>"Yes"]);

                        $rand = random_int(1,3);$used = [];
                        for ($i = 0; $i <= $rand; $i++){
                            $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                            $used[] =$keyword;
                            $article->keywords()->attach($keyword);
                        }
                    });
            });

         **/

        /*
        Point::factory(20)
            ->create()
            ->each(function (Point $po) use ($articlePortraits) {
                Article::factory(1)
                    ->create([
                        'point_id'=>$po->id,
                    ])
                    ->each(function (Article $article) use ($articlePortraits){
                        $basin = Basin::get()->random()->id;
                        $article->basins()->attach($basin);

                        $rand = random_int(3,6);
                        $used = [];
                        for ($i = 0; $i <= $rand; $i++){
                            //KEYWORDS
                            $keyword = Keyword::whereNotIn('id', $used)->get()->random()->id;
                            $used[] =$keyword;
                            $article->keywords()->attach($keyword);
                        }

                        $rand = random_int(1,3);
                        $usedAuthor = [];
                        $usedRivers = [];
                        $usedCategory = [];
                        for ($i = 1; $i <= $rand; $i++){
                            //RIVERS
                            $river = River::whereNotIn('id', $usedRivers)->get()->random()->id;
                            $usedRivers[] =$river;
                            $article->rivers()->attach($river);


                            //AUTHORS
                            $author = Author::whereNotIn('id', $usedAuthor)->get()->random()->id;
                            $usedAuthor[] =$author;
                            $article->authors()->attach($author, ["position"=>$i]);

                            //RELATED IMAGES
                            Document::factory(1)->create([
                                'article_id' => $article->id
                            ]);

                            //CATEGORIES
                            $category = Category::where('parent_category_id', NULL)->whereNotIn('id', $usedCategory)->get()->random()->id;
                            $usedCategory[] =$category;
                            $article->categories()->attach($category);
                            //ADDING SUBCATEGORIES
                            $rand = random_int(0,2);
                            $usedSubCategory = [];
                            for ($i = 0; $i <= $rand; $i++){
                                $subCategory = Category::where('parent_category_id', $category)->whereNotIn('id', $usedSubCategory)->get()->random()->id;
                                $usedSubCategory[] =$subCategory;
                                $article->categories()->attach($subCategory);
                            }

                        }
                        $usedAttribute = [];
                        for ($i = 0; $i <= 10; $i++){
                            $attribute = Attribute::whereNotIn('id', $usedAttribute)->get()->random()->id;
                            $usedAttribute[] =$attribute;

                            $attr_value = fake()->word;
                            $attr_value_en = $attr_value."-en";
                            $article->attributes()->attach($attribute, ["value"=>$attr_value,"value_en"=>$attr_value_en]);
                        }
                        Document::factory(1)->create([
                            'name' => 'portrait',
                            'name_en' => 'portrait',
                            'type' => 'portrait',
                            'link'=> $articlePortraits[$article->id % 2 == 0],
                            'order' => 0,
                            'article_id' => $article->id,
                        ]);
                    });
            });
        */

        /*DOCUMENTS*/
        /*
        $articlePDFdocs = [
            'Credit Fraud Detection Based on Hybrid Credit Scoring Model' => '1-s2.0-S1877050920306402-main.pdf',
            'CSD Based DR Approach for the Solution of Differential Equation of LTI SISO System of Causal Type' => '1-s2.0-S1877050920306426-main.pdf',
            'Musical instrument emotion recognition using deep recurrent neural network' => '1-s2.0-S1877050920306438-main.pdf',
        ];
        foreach ($articlePDFdocs as $doc => $doc_url){
            Document::factory(4)->create([
                'name' => $doc,
                'name_en' => $doc." - en",
                'type' => 'original',
                'link' => $doc_url,
            ]);
        }
        */
        /*DOCUMENTS*/

    }
}

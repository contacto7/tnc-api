<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {


        $categories = [
            'hydrogeomorphology' => 'hidrogeomorfología',
            'biodiversity' => 'biodiversidad',
            'threats' => 'amenazas',
            'conservation' => 'conservación',
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'latitude',
        'longitude',
        'name',
        'article_id',
    ];

    public function article(){
        //return $this->hasMany(Article::class)->orderBy('created_at', 'desc');
        return $this->belongsTo(Article::class);
    }

    public function polygons(){
        return $this
            ->belongsToMany(Polygon::class)
            ->withTimestamps();
    }
}

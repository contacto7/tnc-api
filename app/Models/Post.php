<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'name_en',
        'post_type',
        'access_type',
        'current_content_version',
        'revision_id',
    ];

    public function revision(){
        return $this->belongsTo(Revision::class);
    }
    public function postContents(){
        return $this->hasMany(PostContent::class);
    }

}

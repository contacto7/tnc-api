<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'name_en',
        'description_en',
    ];

    public function users(){
        return $this
            ->belongsToMany(User::class)
            //->withPivot('voted_at', 'state', 'device_uuid', 'ip_registered')
            ->withTimestamps();
    }
}

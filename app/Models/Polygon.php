<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Polygon extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'file',
    ];

    public function points(){
        return $this
            ->belongsToMany(Point::class)
            ->withTimestamps();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    const ACTIVE = 1;
    const APPROVED = 2;
    const FOR_APPROVAL = 3;
    const NON_APPROVED = 4;

    protected $fillable = [
        'id',
        'doi',
        'name',
        'name_en',
        'abstract',
        'abstract_en',
        'point_order',
        'first_published_at',
        'citations',
        'type_id',
        'status',
        'contact_email',
        'contact_cellphone',
        'contact_name',
        'contact_last_name',
    ];

    public function authors(){
        return $this
            ->belongsToMany(Author::class)
            ->withPivot('position')
            ->withTimestamps();
    }
    public function attributes(){
        return $this
            ->belongsToMany(Attribute::class)
            ->withPivot('value', 'value_en')
            ->withTimestamps();
    }
    public function categories(){
        return $this
            ->belongsToMany(Category::class)
            ->withTimestamps();
    }
    public function rivers(){
        return $this
            ->belongsToMany(River::class)
            ->withTimestamps();
    }
    public function basins(){
        return $this
            ->belongsToMany(Basin::class)
            ->withTimestamps();
    }
    public function keywords(){
        return $this
            ->belongsToMany(Keyword::class)
            ->withTimestamps();
    }
    public function points(){
        return $this->hasMany(Point::class);
    }
    public function type(){
        return $this->belongsTo(Type::class);
    }
    public function documents(){
        return $this->hasMany(Document::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostContent extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'slug',
        'name',
        'name_en',
        'value',
        'value_en',
        'post_id',
        'revision_id',
    ];

    public function post(){
        return $this->belongsTo(Post::class);
    }
    public function revision(){
        return $this->belongsTo(Revision::class);
    }
}

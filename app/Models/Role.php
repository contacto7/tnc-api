<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    const DEVELOPER = 1;
    const ADMIN = 2;
    const WORKER = 3;

    public function user(){
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    /**
     Document types:
     -Original: PDF version of the article
     -Portrait th
     * */

    protected $fillable = [
        'name',
        'name_en',
        'type',
        'link',
        'order',
        'article_id',
    ];

    public function article(){
        return $this->belongsTo(Article::class);
    }

}

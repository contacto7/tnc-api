<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    use HasFactory;

    protected $fillable = [
        'point_style',
        'point_image',
        'point_background_color',
        'point_color',
        'point_width',
        'point_height',
        'point_size',
        'line_color',
        'line_width',
    ];

    public function layers(){
        return $this->hasMany(Layer::class);
    }
}

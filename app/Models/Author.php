<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'last_name',
        'email',
        'bio',
        'bio_en',
    ];

    public function articles(){
        return $this
            ->belongsToMany(Article::class)
            ->withPivot('position')
            ->withTimestamps();
    }
}

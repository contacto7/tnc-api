<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Layer extends Model
{
    use HasFactory;



    protected $fillable = [
        'type',
        'layer_type',
        'description',
        'description_en',
        'name',
        'name_en',
        'legend_html',
        'z_order',
        'display_order',
        'geojson_id',
        'style_id',
        'map_id',
    ];

    public function geojson(){
        return $this->belongsTo(Geojson::class);
    }
    public function style(){
        return $this->belongsTo(Style::class);
    }
    public function map(){
        return $this->belongsTo(map::class);
    }
}

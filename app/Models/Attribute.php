<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'name_en',
        'category_id',
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function articles(){
        return $this
            ->belongsToMany(Article::class)
            ->withPivot('value', 'value_en')
            ->withTimestamps();
    }
}

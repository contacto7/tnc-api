<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'name_en',
        'description_en',
    ];

    public function layers(){
        return $this->hasMany(Layer::class);
    }
}

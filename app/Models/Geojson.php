<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Geojson extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'name',
        'name_en',
        'file',
    ];

    public function layers(){
        return $this->hasMany(Layer::class);
    }
}

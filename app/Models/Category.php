<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const HYDRO = 1;
    const BIODIVERSITY = 2;
    const THREATS = 3;
    const CONSERVATION = 4;

    use HasFactory;

    protected $fillable = [
        'name',
        'name_en',
        'parent_category_id',
    ];

    public function articles(){
        return $this
            ->belongsToMany(Article::class)
            ->withPivot('value', 'value_en')
            ->withTimestamps();
    }
    public function attributes(){
        return $this->hasMany(Attribute::class);
    }
}

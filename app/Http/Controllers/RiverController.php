<?php

namespace App\Http\Controllers;

use App\Models\River;
use App\Http\Requests\StoreRiverRequest;
use App\Http\Requests\UpdateRiverRequest;

class RiverController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rivers = River::get();

        return $rivers;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRiverRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(River $river)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(River $river)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRiverRequest $request, River $river)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(River $river)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Geojson;
use App\Http\Requests\StoreGeojsonRequest;
use App\Http\Requests\UpdateGeojsonRequest;

class GeojsonController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreGeojsonRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Geojson $geojson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Geojson $geojson)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateGeojsonRequest $request, Geojson $geojson)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Geojson $geojson)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Basin;
use App\Http\Requests\StoreBasinRequest;
use App\Http\Requests\UpdateBasinRequest;

class BasinController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $basins = Basin::get();

        return $basins;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBasinRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Basin $basin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Basin $basin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBasinRequest $request, Basin $basin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Basin $basin)
    {
        //
    }
}

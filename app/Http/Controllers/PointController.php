<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Author;
use App\Models\Keyword;
use App\Models\Point;
use App\Http\Requests\StorePointRequest;
use App\Http\Requests\UpdatePointRequest;
use App\Models\Polygon;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {/*
        $articles = Point::with([
            'articles.keywords',
            'articles.authors',
            'articles.type',
            'articles.categories',
            'articles.attributes',
            'articles.basins',
            'articles.rivers',
        ])->get();*/

        return (Point::with([])->get());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePointRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show($point)
    {
        /*
        return Point::where('id',$point)->with([
            'articles.keywords',
            'articles.authors',
            'articles.documents',
            'articles.type',
            'articles.categories',
            'articles.attributes',
            'articles.basins',
            'articles.rivers',
        ])->first();*/
        $point =  Point::where('id',$point)->first();
        $article = $point->article()->with([
            'keywords',
            'authors',
            'documents',
            'type',
            'categories',
            'attributes',
            'basins',
            'rivers',
            'points.polygons',
        ])->first();
        $articleToUpdate = $point->article()->with([
            'keywords',
            'authors',
            'documents',
            'type',
            'categories',
            'attributes',
            'basins',
            'rivers',
            'points.polygons',
        ])->first();

        if($article && $article->doi){
            $url = 'http://api.crossref.org/works/'.$article->doi;
            $response = Http::get($url);
            $response = json_decode($response->getBody()->getContents(),true);
            $message = $response["message"];

            $article->name = $this->putAPIdata($article, $message, "title", "name", 0);
            $article->name_en = $this->putAPIdata($article, $message, "title", "name_en", 0);
            $article->abstract = $this->putAPIdata($article, $message, "abstract", "abstract");
            $article->abstract_en = $this->putAPIdata($article, $message, "abstract", "abstract");
            $article->citations = $this->putAPIdata($article, $message, "is-referenced-by-count", "citations");
            $article->first_published_at = $this->putAPIdata($article, $message, "created", "first_published_at", "date-time");
            $article->first_published_at = date('d-m-Y', strtotime($article->first_published_at));

            $authors = $message["author"];
            $authorInternal = [];
            $temp = 1;
            foreach ($authors as $author){
                $tempAuthor = new \stdClass();
                if(!empty($author["given"])){
                    $tempAuthor->name = $author["given"];
                }
                if(!empty($author["family"])){
                    $tempAuthor->last_name = $author["family"];
                }
                if(!empty($author["email"])){
                    $tempAuthor["email"] = $author["email"];
                }
                if(!empty($author["bio"])){
                    $tempAuthor["bio"] = $author["bio"];
                }
                if(!empty($author["bio"])){
                    $tempAuthor["bio_en"] = $author["bio"];
                }
                $tempAuthor->pivot = new \stdClass();

                $tempAuthor->pivot->position = $temp;
                $temp++;

                array_push($authorInternal, $tempAuthor);

                /*
                $concatenatedString = $author["given"] . $author["family"];
                $md5Hash = md5($concatenatedString);
                $authorModel = Author::where('email', $md5Hash."@mail.com")->first();
                if(!$authorModel){
                    $authorModel = Author::create([
                        "name" => $author["given"],
                        "last_name" => $author["family"],
                        "email" => $md5Hash."@mail.com",

                    ]);
                }
                $article->authors()->syncWithoutDetaching($authorModel);

                $articleToUpdate->update([
                    "name" => $message["title"][0],
                    "name_en" => $message["container-title"][0],
                ]);*/

            }

            $type = null;
            if(!empty($message["type"] )){
                $type = new \stdClass();
                $type->name = ucfirst(str_replace("-"," ",$message["type"]));
                $type->name_en = ucfirst(str_replace("-"," ",$message["type"]));
            }


            $article =  $article->toArray();
            $article["authors"] = $authorInternal;
            if($type){
                $article["type"] = $type;
            }
        }



        $point =  $point->toArray();
        $dataMerged = array_merge($point, [
            "article" => $article
        ]);
        return $dataMerged;

    }

    public function putAPIdata($article, $message, $apiField, $internalField, $arrayIndex = NULL){
        if(!empty($message[$apiField] )){
            if($arrayIndex !== NULL){
                if(!empty($message[$apiField][$arrayIndex])){
                    return $message[$apiField][$arrayIndex];
                }
            }
            return $message[$apiField];
        }
        return $article->{$internalField};
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Point $point)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePointRequest $request, Point $point)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Point $point)
    {
        //
    }
}

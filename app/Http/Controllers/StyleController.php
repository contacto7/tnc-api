<?php

namespace App\Http\Controllers;

use App\Models\Style;
use App\Http\Requests\StoreStyleRequest;
use App\Http\Requests\UpdateStyleRequest;

class StyleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStyleRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Style $style)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Style $style)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStyleRequest $request, Style $style)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Style $style)
    {
        //
    }
}

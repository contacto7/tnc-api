<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Models\Point;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $articles = Article::where('id', '>', 0)->with([
            'keywords',
            'categories',
            'attributes',
            'authors',
            'basins',
            'rivers',
            'type',
            'documents',
            'points.polygons',
        ])->limit(60)->get();

        return $articles;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $aaa = null;
        $doi = $request->input('doi');
        $name = $request->input('name');
        $abstract = $request->input('abstract');
        $published_at = $request->input('published_at');
        $citations = $request->input('citations');
        $keywords = $request->input('keywords');
        $authors = $request->input('authors');
        $basins = $request->input('basins');
        $rivers = $request->input('rivers');
        $topics = $request->input('topics');
        $polygons = $request->input('polygons');

        $url = 'http://api.crossref.org/works/'.$doi;
        $response = Http::get($url);
        $response = json_decode($response->getBody()->getContents(),true);
        if($response && isset($response["message"])){
            $message = $response["message"];

            if(!$name){
                $name = $this->putAPIdata($message, "title", 0);
            }
            if(!$abstract){
                $abstract = $this->putAPIdata($message, "abstract");
            }
            if(!$published_at){
                $published_at = $this->putAPIdata($message, "created", "date-time");
                $published_at = date('d/m/Y', strtotime($published_at));
            }
            if(!$citations){
                $citations = $this->putAPIdata($message, "is-referenced-by-count");
            }
        }

        $date = Carbon::createFromFormat('d/m/Y', $published_at);
        $mysqlDateTime = $date->format('Y-m-d H:i:s');

        $article = Article::create([
            'doi' => $doi,
            'name' => $name,
            'name_en' => $name,
            'abstract' => $abstract,
            'abstract_en' => $abstract,
            'point_order' => 1,
            'first_published_at' => $mysqlDateTime,
            'citations' => $citations,
            'type_id' => 1,
            'status' => 1,
            'contact_name' => null,
            'contact_last_name' => null,
            'contact_email' => null,
            'contact_cellphone' => null,
        ]);

        foreach ($polygons as $polygon){
            $polygon_id = $polygon["polygon"];
            $point = Point::create([
                'latitude' => $polygon["lat"],
                'longitude' => $polygon["long"],
                'name' => "point",
                'article_id' => $article->id ,
            ]);
            $point->polygons()->attach([$polygon_id]);
        }

        $article->keywords()->attach($keywords);
        $article->authors()->attach($authors);
        $article->basins()->attach($basins);
        $article->rivers()->attach($rivers);
        $article->categories()->attach($topics);


        return response()->json([
            "article" => $article,
        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show($article)
    {
        return Article::where('id',$article)->with([
            'keywords',
            'categories',
            'attributes',
            'authors',
            'basins',
            'rivers',
            'type',
            'documents',
            'points',
        ])->first();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateArticleRequest $request, Article $article)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Article $article)
    {
        //
    }

    public function putAPIdata($message, $apiField, $arrayIndex = NULL){
        if(!empty($message[$apiField] )){
            if($arrayIndex !== NULL){
                if(!empty($message[$apiField][$arrayIndex])){
                    return $message[$apiField][$arrayIndex];
                }
            }
            return $message[$apiField];
        }
        return null;
    }
}

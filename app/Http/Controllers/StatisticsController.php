<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Basin;
use App\Models\Category;
use App\Models\Keyword;
use App\Models\Point;
use App\Models\Polygon;
use App\Models\River;
use Carbon\Carbon;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function articlesPerSubject()
    {
        $hydro = Article::whereHas('categories', function($q){
            $q->where('id', Category::HYDRO);
        })->count();
        $bio = Article::whereHas('categories', function($q){
            $q->where('id', Category::BIODIVERSITY);
        })->count();
        $threats = Article::whereHas('categories', function($q){
            $q->where('id', Category::THREATS);
        })->count();
        $conservation = Article::whereHas('categories', function($q){
            $q->where('id', Category::CONSERVATION);
        })->count();
        return [
            "hydro" => $hydro,
            "bio" => $bio,
            "threats" => $threats,
            "conservation" => $conservation,
        ];
    }
    public function articlesPerBasin()
    {
        $basins = Basin::withCount('articles')
            /*THIS IS TO GET ONLY WHERE ARTICLES COUNT GREATER THAN 0*/
            ->whereHas('articles')
            ->orderBy('articles_count', 'DESC')
            /*THIS IS TO GET ONLY WHERE ARTICLES COUNT GREATER THAN 0*/
            ->get();

        $result = $basins->pluck('articles_count', 'name');

        return $result;
    }
}
